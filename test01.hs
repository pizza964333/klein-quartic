import Data.Complex
import System.Random

test = do
  x <- randomComplex
  y <- randomComplex
  let z = fun x y
  let q = alg x y z
  print (x,y,z,q)

randomComplex :: IO (Complex Double)
randomComplex = do
  a <- randomRIO (-9,9)
  b <- randomRIO (-9,9)
  return (a :+ b)

alg x y z = x**3*y + y**3*z + z**3*x

fun :: Complex Double -> Complex Double -> Complex Double
fun x y = sq3 / ( r3t2 * p3w2d3 * x ) - (r3t2d3 * y**3) / sq3
 where
  sqr3 = sqrt 3
  r3t2  = 2 ** (1/3)
  r3t2d3 = (2/3) ** (1/3)
  p3w2d3 = 3 ** (2/3)
  v9x5y   = 9 * x**5 * y
  v4x3y9  = 4 * x**3 * y**9
  v27x10y2 = 27 * x ** 10 * y**2
  sq3 = ( sqr3 * sqrt ( v27x10y2 + v4x3y9 ) - v9x5y ) ** (1/3)

